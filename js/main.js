import GameApp from './App';

let initGame = () => {
        const config = {width:800,height:600,bg:0xcccccc,x:100,y:100,side:50, cols:6, rows:6};
        let app = new GameApp(config);
        app.init();
        let mobile = isMobile();
        if (mobile) {window.scrollTo(0, 1);}
}

function isMobile() {
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}
window.onload = initGame;