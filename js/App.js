import GameScene from './GameScene';

export default class GameApp {

    constructor(config){
        this.stage = new GameScene(config).getScene();
        this.config = config;
        this.selectedNumbers = [];
        this.winNumbers = [];
        this.lukyNumbers = [];
        this.maxWinNumbers = 6;
        this.maxRandomNumbers = 12;
        this.wins = [0, 2, 5, 20, 50, 200, 1000];
        this.gameEnd = false;
    }

    init(){
        this.gridContainer = new PIXI.Container();
        this.mainController = new PIXI.Container();
        this.selectedContainer = new PIXI.Container();
        this.resultContainer = new PIXI.Container();
        this.selectedContainer.x = this.config.x + (this.config.cols * this.config.side) + this.config.side;
        this.stage.addChild(this.mainController);
        this.mainController.addChild(this.gridContainer);
        this.mainController.addChild(this.selectedContainer);

        this.generateWinNumbers();
        this.initView();
    }

    initView(){

        let instruction = new PIXI.Text(`Choose 6 numbers`);
        instruction.x = this.config.x;
        instruction.y = 20;
        this.mainController.addChild(instruction);

        this.makeGrid();
        this.prepareResultModal();
    }

    prepareResultModal(){
        let modal = new PIXI.Graphics();
        modal.lineStyle(3, 0x000000, 1);
        modal.beginFill(0xFFFFFF, 1);
        modal.drawRect(0, 0, 600, 400);
        modal.endFill();
        this.resultContainer.addChild(modal);
        this.resultContainer.x = this.config.width / 2 - 300;
        this.resultContainer.y = this.config.height / 2 - 200;
    }

    makeGrid(){
        let number, graphics;
        let n = 1;
        for (let r = 0; r < this.config.rows; r++){
            for (let c = 0; c < this.config.cols; c++){
                const x = this.config.x + (this.config.side * c);
                const y = this.config.y + (this.config.side * r);
                number = new PIXI.Text(`${n}`);
                number.x = x + this.config.side / 2;
                number.anchor.x = 0.5;
                number.y = y + 10;

                graphics = new PIXI.Graphics();
                graphics.lineStyle(2, 0x000000, 1);
                graphics.beginFill(0xFFFFFF, 1);
                graphics.drawRect(x, y, this.config.side, this.config.side);
                graphics.endFill();
                graphics.id = n;
                graphics.interactive = true;
                graphics.on('click', this.selectNumber, this);
                this.gridContainer.addChild(graphics);
                this.gridContainer.addChild(number);
                n++;
            }
        }
        this.makeSelected();
    }

    selectNumber(event){
        if(this.gameEnd) return;
        this.selectedNumbers.push(event.target.id);
        event.target.interactive = false;
        event.target.alpha = 0.3;
        this.showSelected();
        if(this.selectedNumbers.length == this.maxWinNumbers){
            this.checkResult();
        }
    }

    makeSelected(){
        let selectedTitle = new PIXI.Text(`Selected numbers`);
        selectedTitle.x = 0;
        selectedTitle.y = 20;
        this.selectedContainer.addChild(selectedTitle);

        let graphics;
        for (let c = 0; c < this.config.rows; c++){
                const x = this.config.side * c;
                graphics = new PIXI.Graphics();
                graphics.lineStyle(2, 0x000000, 1);
                graphics.beginFill(0xffffff, 1);
                graphics.drawCircle(x, this.config.y + this.config.side / 2, this.config.side / 2);
                graphics.endFill();
                this.selectedContainer.addChild(graphics);
        }
    }

    generateWinNumbers(){
        while (this.winNumbers.length < this.maxRandomNumbers){
            let random = Math.floor(Math.random() * (this.config.cols * this.config.rows) + 1);
            if(this.winNumbers.indexOf(random) === -1){
                this.winNumbers.push(random);
            }
        }
    }

    showSelected(){
        const index = this.selectedNumbers.length - 1;
        const number = this.selectedNumbers[index];
        let title = new PIXI.Text(`${number}`);
        title.y = this.config.y + 10;
        this.selectedContainer.addChild(title);
        title.x = (this.config.side * index);
        title.anchor.x = 0.5;
    }

    gameOver(result){
        this.gameEnd = true;
        this.mainController.alpha = 0;

        let circle, winNumber;

        let winCombinationTitle = new PIXI.Text(`Win combination`);
        winCombinationTitle.x = 300;
        winCombinationTitle.anchor.x = 0.5;
        winCombinationTitle.y = 30;
        this.resultContainer.addChild(winCombinationTitle);

        for (let c = 0; c < this.maxRandomNumbers; c++){
            const x = this.config.side * c + this.config.side / 2;
            circle = new PIXI.Graphics();
            circle.lineStyle(2, 0x000000, 1);
            circle.beginFill(0xffffff, 1);
            circle.drawCircle(x, 100, this.config.side / 2);
            circle.endFill();
            this.resultContainer.addChild(circle);

            winNumber = new PIXI.Text(this.winNumbers[c]);
            winNumber.x = x;
            winNumber.anchor.x = 0.5;
            winNumber.y = 80;
            this.resultContainer.addChild(winNumber);
        }

        let userNumbers = new PIXI.Container();
        this.resultContainer.addChild(userNumbers);

        for (let c = 0; c < this.lukyNumbers.length; c++){
            const x = this.config.side * c + this.config.side / 2;
            circle = new PIXI.Graphics();
            circle.lineStyle(2, 0x000000, 1);
            circle.beginFill(0xffffff, 1);
            circle.drawCircle(x, 230, this.config.side / 2);
            circle.endFill();
            userNumbers.addChild(circle);

            winNumber = new PIXI.Text(this.lukyNumbers[c]);
            winNumber.x = x;
            winNumber.anchor.x = 0.5;
            winNumber.y = 210;
            userNumbers.addChild(winNumber);
        }
        userNumbers.x = this.resultContainer.width / 2 - userNumbers.width / 2;

        let matchTitle = new PIXI.Text(`Match numbers: ${this.lukyNumbers.length} of ${this.selectedNumbers.length}`);
        matchTitle.x = 300;
        matchTitle.anchor.x = 0.5;
        matchTitle.y = 150;
        this.resultContainer.addChild(matchTitle);

        let winAmount = new PIXI.Text(`You win: ${this.wins[result]}`);
        winAmount.x = 300;
        winAmount.anchor.x = 0.5;
        winAmount.y = 270;
        this.resultContainer.addChild(winAmount);

        let resetButton = new PIXI.Graphics();
        resetButton.lineStyle(2, 0x000000, 1);
        resetButton.beginFill(0x00FF00, 1);
        resetButton.drawRect(this.resultContainer.width / 2 - 100, 320, 200, 50);
        resetButton.endFill();
        this.resultContainer.addChild(resetButton);
        resetButton.interactive = true;
        resetButton.on('click', this.restartGame, this);

        let resetText = new PIXI.Text(`Play again`);
        resetText.x = this.resultContainer.width / 2;
        resetText.anchor.x = 0.5;
        resetText.y = 330;
        this.resultContainer.addChild(resetText);

        this.stage.addChild(this.resultContainer);
    }

    restartGame(){
        this.resultContainer.removeChildren();
        this.gridContainer.removeChildren();
        this.selectedContainer.removeChildren();
        this.stage.removeChild(this.resultContainer);
        this.mainController.alpha = 1;
        this.selectedNumbers = [];
        this.winNumbers = [];
        this.lukyNumbers = [];
        this.gameEnd = false;
        this.generateWinNumbers();
        this.makeGrid();
    }

    checkResult(){
        let result = 0;
        this.selectedNumbers.map( n => {
           if(this.winNumbers.indexOf(n) !== -1){
               this.lukyNumbers.push(n);
               result++;
           }
        });

      this.gameOver(result);
    }
}